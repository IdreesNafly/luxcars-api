package com.example.eea.luxcars;

import com.example.eea.luxcars.ImageUpload.StorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class LuxcarsApplication {

    public static void main(String[] args) {
        SpringApplication.run(LuxcarsApplication.class, args);
    }

}
