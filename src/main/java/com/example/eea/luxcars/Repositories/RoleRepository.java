package com.example.eea.luxcars.Repositories;

import com.example.eea.luxcars.Entities.ERole;
import com.example.eea.luxcars.Entities.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RoleRepository extends CrudRepository<Role,Integer> {
    Optional <Role> findByName(ERole name);
}
