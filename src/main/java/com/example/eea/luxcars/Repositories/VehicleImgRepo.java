package com.example.eea.luxcars.Repositories;

import com.example.eea.luxcars.Entities.Vehicle;
import com.example.eea.luxcars.Entities.VehicleImages;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VehicleImgRepo extends CrudRepository<VehicleImages,Long> {
    public VehicleImages findByVehicleAndImagePath(Vehicle vehicle, String path);
    List <VehicleImages> findAllByVehicle(Vehicle vehicle);
    VehicleImages findByImgName(String name);
}
