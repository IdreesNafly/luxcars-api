package com.example.eea.luxcars.Repositories;

import com.example.eea.luxcars.Entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends CrudRepository<User,String> {
    Optional<User> findByEmail(String email);
    Boolean existsByEmail(String email);

}
