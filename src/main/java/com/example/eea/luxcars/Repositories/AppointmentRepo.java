package com.example.eea.luxcars.Repositories;

import com.example.eea.luxcars.Entities.Appointments;
import com.example.eea.luxcars.Entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppointmentRepo extends CrudRepository<Appointments,Long> {
    List <Appointments> findAllByUser(User user);
}
