package com.example.eea.luxcars.Repositories;

import com.example.eea.luxcars.Entities.Reviews;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewRepo extends CrudRepository <Reviews,Long> {

//    List<Reviews> findByVehicle(Vehicle vehicle);

}
