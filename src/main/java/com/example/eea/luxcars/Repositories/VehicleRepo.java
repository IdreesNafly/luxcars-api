package com.example.eea.luxcars.Repositories;

import com.example.eea.luxcars.Entities.User;
import com.example.eea.luxcars.Entities.Vehicle;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VehicleRepo extends CrudRepository<Vehicle,Long> {

    List <Vehicle> findAllByUser(User user);
    List <Vehicle> findAllByIsAdTrue();
    List <Vehicle> findAllByReservedBy(String email);
    List <Vehicle> findAllByVehicleMakeAndVehicleModelAndVehicleYom(String make, String model, String year);
    List <Vehicle> findAllByVehicleYomGreaterThanEqual(String yom);
    List <Vehicle> findAllByVehicleMake(String make);
    List <Vehicle> findAllByVehicleModel(String model);
    List <Vehicle> findAllByVehicleMakeAndVehicleModel(String make, String model);
    List <Vehicle> findAllByVehicleType(String type);

    List <Vehicle> findAllByStatus(String status);

    @Query("select veh.vehicleMake from Vehicle veh")
    List<String> getAllVehicleMake();

    @Query("select veh.vehicleModel from Vehicle veh")
    List<String> getAllVehicleModel();

    @Query("select veh.vehicleYom from Vehicle veh")
    List<String> getAllVehicleYear();


}
