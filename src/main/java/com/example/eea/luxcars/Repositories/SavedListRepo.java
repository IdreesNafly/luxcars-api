package com.example.eea.luxcars.Repositories;

import com.example.eea.luxcars.Entities.SavedList;
import com.example.eea.luxcars.Entities.User;
import com.example.eea.luxcars.Entities.Vehicle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SavedListRepo extends CrudRepository<SavedList,Long> {

    List <SavedList> findAllByUser(User user);
    List <SavedList> findByUserAndVehicle(User user, Vehicle vehicle);

}
