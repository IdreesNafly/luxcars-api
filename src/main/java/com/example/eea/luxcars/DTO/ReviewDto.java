package com.example.eea.luxcars.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ReviewDto {


    private long id;

    private String description;
    //private String header;
    private double rating;
    private String userid;
    private String createdDate;

    public ReviewDto(long id, String description, double rating, String userid) {
        this.id = id;
        this.description = description;
        this.rating = rating;
        this.userid = userid;
    }
}
