package com.example.eea.luxcars.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class VehicleDto {

    private long vehicleId;
    private double vehiclePrice=0.0;
    private String status;
    private String reservedBy;

    private boolean isAd=false;
    private boolean isApproved=false;

    public VehicleDto(long vehicleId, boolean isAd, boolean isApproved) {
        this.vehicleId = vehicleId;
        this.isAd = isAd;
        this.isApproved = isApproved;
    }

    public VehicleDto(long vehicleId, double vehiclePrice) {
        this.vehicleId = vehicleId;
        this.vehiclePrice = vehiclePrice;
    }

    public VehicleDto(long vehicleId, String status) {
        this.vehicleId = vehicleId;
        this.status = status;
    }

    public VehicleDto(long vehicleId, String status, String reservedBy) {
        this.vehicleId = vehicleId;
        this.status = status;
        this.reservedBy = reservedBy;
    }
}
