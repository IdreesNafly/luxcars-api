package com.example.eea.luxcars.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Column;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VehicleAd {

    private long id;
    private String vehicleMake;
    private String vehicleModel;
    private String vehicleType;
    private String vehicleExteriorColor;
    private String vehicleInteriorColor;
    private String vehicleYom;
    private MultipartFile[] images;
//    private MultipartFile coverimg;

    @Column(columnDefinition = "LONGTEXT")
    private String vehicleDescription;

    private String vehicleFuel;
    private String vehicleTransmission;
    private String vehicleMileage;
    private double vehiclePrice=0.0;

    private String userid;
}
