package com.example.eea.luxcars.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AppointmentDto {

    private long id;

    private String message;

    private Date appointmentDateTime;

    private String status;

    private long vehicleid;

    private String useremail;


    public AppointmentDto(long id, String status) {
        this.id = id;
        this.status = status;
    }

    public AppointmentDto(String message, Date appointmentDateTime, long vehicleid, String useremail) {
        this.message = message;
        this.appointmentDateTime = appointmentDateTime;
        this.vehicleid = vehicleid;
        this.useremail = useremail;
    }
}
