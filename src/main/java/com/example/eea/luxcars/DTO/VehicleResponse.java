package com.example.eea.luxcars.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class VehicleResponse {
    private long id;
    private String vehicleMake;
    private String vehicleModel;
    private String vehicleType;
    private String vehicleExteriorColor;
    private String vehicleInteriorColor;
    private String vehicleYom;
    private String vehicleDescription;
    private String vehicleFuel;
    private String vehicleTransmission;
    private String vehicleMileage;
    private Double vehiclePrice;
    private String status;
    private Boolean isAd;
    private Boolean isAdApproved;
    private String covrimg;
    private String reservedBy;
    private List<String> imageList=new ArrayList<>();
}
