package com.example.eea.luxcars.Entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="vehicle_images")
public class VehicleImages {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long imageid;

    private String imagePath;

    @Lob
    private byte[] fileData;

    private String imgName;
    private String imgType;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "vehicleid",referencedColumnName = "id")
    private Vehicle vehicle;
}
