package com.example.eea.luxcars.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.*;


@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Table(name = "vehicle")
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false , unique = true)
    private long id;

    private String vehicleMake;
    private String vehicleModel;
    private String vehicleType;
    private String vehicleExteriorColor;
    private String vehicleInteriorColor;
    private String vehicleYom;

    @Column(columnDefinition = "LONGTEXT")
    private String vehicleDescription;

    private String vehicleFuel;
    private String vehicleTransmission;
    private String vehicleMileage;
    private double vehiclePrice=0.0;
    private String status;

    private String reservedBy;

    private Boolean isAd=false;
    private Boolean isAdApproved=false;

    private Date reservedDate;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="userid")
    private User user;

    String images;
    private String covrimg;

    @OneToMany(mappedBy = "vehicle", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private List<SavedList> savedLists = new ArrayList<>();

    @OneToMany(mappedBy = "vehicle",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List <Appointments> appointments=new ArrayList<>();

    @OneToMany(mappedBy = "vehicle",cascade = CascadeType.REMOVE)
    List <VehicleImages> vehicleImages;



    public List<String> retrieveImagesArray() {
        if (StringUtils.hasText(this.images)) {
            return Arrays.asList(this.images.split(","));
        }
        return Collections.emptyList();
    }

    public void setImagesFromArray(List<String> images) {
        this.images = String.join(",", images);
    }

}
