package com.example.eea.luxcars.Entities;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
