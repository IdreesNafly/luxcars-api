package com.example.eea.luxcars.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "user")
public class User {


    @Id
    @Column(nullable = false,unique = true)
    @Email
    private String email;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    private String fullName;
    private String contactNo;


    @CreationTimestamp
    @JsonIgnore
    private Date createdAt;

    @JsonIgnore
    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List <Reviews> reviews=new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List <Appointments> appointments=new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List <Vehicle> vehicles=new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    @JsonIgnore
    private List <SavedList> savedLists;


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(	name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    public User(@Email String email, String password) {
        this.email = email;
        this.password = password;

    }

    public User(@Email String email, String password, String fullName, String contactNo) {
        this.email = email;
        this.password = password;
        this.fullName = fullName;
        this.contactNo = contactNo;
    }

    public User(@Email String email, String password, String fullName, String contactNo, Set<Role> roles) {
        this.email = email;
        this.password = password;
        this.fullName = fullName;
        this.contactNo = contactNo;
        this.roles = roles;
    }
}
