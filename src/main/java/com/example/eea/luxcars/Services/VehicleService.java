package com.example.eea.luxcars.Services;

import com.example.eea.luxcars.DTO.VehicleDto;
import com.example.eea.luxcars.DTO.VehicleResponse;
import com.example.eea.luxcars.EmailService.EmailModel;
import com.example.eea.luxcars.EmailService.EmailService;
import com.example.eea.luxcars.Entities.User;
import com.example.eea.luxcars.Entities.Vehicle;
import com.example.eea.luxcars.ImageUpload.StorageService;
import com.example.eea.luxcars.Repositories.UserRepo;
import com.example.eea.luxcars.Repositories.VehicleImgRepo;
import com.example.eea.luxcars.Repositories.VehicleRepo;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class VehicleService {

    @Autowired
    VehicleRepo vehicleRepository;

    @Autowired
    UserRepo userRepo;

    @Autowired
    VehicleImgRepo vehicleImgRepo;

    @Autowired
    private StorageService storageService;

    @Autowired
    private EmailService emailService;
//    @Autowired
//    SavedListRepo savedListRepo;

    public VehicleService(VehicleRepo vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    public List getAllVehicle(){
        List <Vehicle> vehicleList=new ArrayList<>();
        vehicleRepository.findAll().forEach(vehicle->{

            vehicleList.add(vehicle);});
        return vehicleList;
    }

    public List getAllVehicleResponse(){
        List <VehicleResponse> vehicleList=new ArrayList<>();
        vehicleRepository.findAll().forEach(vehicle->{

            VehicleResponse vehicleResponse=new VehicleResponse();
            vehicleResponse.setId(vehicle.getId());
            vehicleResponse.setVehicleMake(vehicle.getVehicleMake());
            vehicleResponse.setVehicleModel(vehicle.getVehicleModel());
            vehicleResponse.setVehicleType(vehicle.getVehicleType());
            vehicleResponse.setVehicleExteriorColor(vehicle.getVehicleExteriorColor());
            vehicleResponse.setVehicleInteriorColor(vehicle.getVehicleInteriorColor());
            vehicleResponse.setVehicleYom(vehicle.getVehicleYom());
            vehicleResponse.setVehicleDescription(vehicle.getVehicleDescription());
            vehicleResponse.setVehicleFuel(vehicle.getVehicleFuel());
            vehicleResponse.setVehicleTransmission(vehicle.getVehicleTransmission());
            vehicleResponse.setVehicleMileage(vehicle.getVehicleMileage());
            vehicleResponse.setVehiclePrice(vehicle.getVehiclePrice());
            vehicleResponse.setStatus(vehicle.getStatus());
            vehicleResponse.setReservedBy(vehicle.getReservedBy());
            vehicleResponse.setIsAd(vehicle.getIsAd());
            vehicleResponse.setIsAdApproved(vehicle.getIsAdApproved());
            vehicleResponse.setCovrimg(vehicle.getCovrimg());
            vehicleResponse.setImageList(vehicle.retrieveImagesArray());

            vehicleList.add(vehicleResponse);});
        return vehicleList;
    }


    public ResponseEntity<Boolean> deleteVehicle(long id) {
        Optional <Vehicle> vehicleOptional=vehicleRepository.findById(id);

        if(vehicleOptional.isPresent()){
            Vehicle vehicle=vehicleOptional.get();
            if(vehicle.getStatus().equals("Available")){
                vehicleRepository.deleteById(id);
                return new ResponseEntity<>(true, HttpStatus.OK);
            }
            return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(false, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    //    @PostMapping("/upload-file")
    public String uploadFile(MultipartFile file) {

        String name = storageService.store(file);
        String uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/download/")
                .path(name)
                .toUriString();

        //FileResponse fileResponse=new FileResponse(name, uri, file.getContentType(), file.getSize());
//        return new ResponseEntity<>(fileResponse, HttpStatus.OK);
        return uri;
    }
    //    @PostMapping("/upload-multiple-files")
    @ResponseBody
    public List<String> uploadMultipleFiles(MultipartFile[] files) {
        return Arrays.stream(files)
                .map(file -> uploadFile(file))
                .collect(Collectors.toList());
    }
    public Boolean addVehicle(Vehicle vehicle, String email, MultipartFile [] files){
        Optional<Vehicle> vehicleOptional=vehicleRepository.findById(vehicle.getId());

        if(!vehicleOptional.isPresent()){

            if(email!=null){
                Optional <User> userOptional=userRepo.findById(email);
                User user=userOptional.get();
                vehicle.setUser(user);
                vehicle.setIsAdApproved(false);
                vehicle.setIsAd(true);
            }

            vehicle.setCovrimg(uploadFile(files[0]));
            vehicle.setImagesFromArray(uploadMultipleFiles(files));
            vehicle.setStatus("Available");
            vehicleRepository.save(vehicle);
            return true;
        }
        return false;
    }

    private String saveImage(String subPath, String vehicleModel, String imagePath) {
        String path = System.getProperty("user.dir") + subPath;
        String imageName = vehicleModel + ".png";

        File file = new File(path + imageName);
        int count = 1;
        while (file.exists()) {
            imageName = vehicleModel + "_" + count + ".png";
            file = new File(path + imageName);
            count++;
        }

        String base64 = imagePath.split(",")[1];
        byte[] data = Base64.decodeBase64(base64);

        try {
            FileOutputStream out = new FileOutputStream(new File(path + imageName));
            out.write(data);
            out.flush();
            out.close();
            return imageName;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
        }
        return "Saved";
    }

    public Boolean updateVehiclePrice(VehicleDto vehicleDTO){
        //return new ResponseEntity<>(true)
        Optional <Vehicle> vehicle=vehicleRepository.findById(vehicleDTO.getVehicleId());
        if(vehicle.isPresent())
        {
            Vehicle vehicle1=vehicle.get();
            if(vehicle1.getStatus().equals("Available")) {
                vehicle1.setVehiclePrice(vehicleDTO.getVehiclePrice());
                vehicleRepository.save(vehicle1);

                return true;
            }
            return false;
        }
        return false;
    }

    public Vehicle findVehicleById(long id){
        Optional<Vehicle> vehicle= vehicleRepository.findById(id);
        Vehicle vehicle1=vehicle.get();

        return vehicle1;
    }

    public Boolean updateVehicleStatus(VehicleDto vehicleDTO){
        Optional <Vehicle> vehicle=vehicleRepository.findById(vehicleDTO.getVehicleId());
        if(vehicle.isPresent())
        {
            Vehicle vehicle1=vehicle.get();
            if(vehicleDTO.getStatus().equals("Sold")){
                vehicle1.setStatus(vehicleDTO.getStatus());
                vehicleRepository.save(vehicle1);
                return true;
            }

//            if(vehicleDTO.getStatus().equals("Available")){
//                vehicle1.setStatus(vehicleDTO.getStatus());
//                vehicleRepository.save(vehicle1);
//                return true;
//            }

            if(vehicleDTO.getStatus().equals("Reserved")) {
                vehicle1.setStatus(vehicleDTO.getStatus());
                vehicle1.setReservedBy(vehicleDTO.getReservedBy());
                vehicle1.setReservedDate(new Date());
                vehicleRepository.save(vehicle1);
                EmailModel emailModel = new EmailModel();
                emailModel.setSubject("Luxcars Internation: Your Vehicle is Reserved");
                emailModel.setEmail(vehicleDTO.getReservedBy());
                String body = "The following vehicle has been reserved. Please head to the store or contact us at (94772350944) within 3 business days to retain your reservation.Vehicle Details are as follows :" + vehicle1.toString();
                emailModel.setBody(body);
                emailService.sendEmail(emailModel);

                return true;
            }
        }
        return false;
    }

    public Boolean approveAd(long id){

        Optional <Vehicle> vehicleOptional=vehicleRepository.findById(id);

        if(vehicleOptional.isPresent()){
            Vehicle vehicle=vehicleOptional.get();
            vehicle.setIsAdApproved(true);
            vehicleRepository.save(vehicle);
            return true;
        }
        return false;
    }




    public List<Vehicle> getAllVehiclebyUser(String email){
        Optional <User> userOptional=userRepo.findById(email);

        if(userOptional.isPresent()){
            User user=userOptional.get();
            List<Vehicle> vehicleList=vehicleRepository.findAllByUser(user);
            return  vehicleList;
        }
        return null;
    }

    //export const ViewAllAdVehicles=()=>{};

    public ResponseEntity<?> viewAllAdVehicles(){
        List <Vehicle> vehicleList=new ArrayList<>();
        vehicleRepository.findAllByIsAdTrue().forEach(vehicle -> vehicleList.add(vehicle));
        return new ResponseEntity<>(vehicleList,HttpStatus.OK);
    }

    public ResponseEntity<?> getReservedVehicles(String email){

        Optional <User> userOptional=userRepo.findById(email);
        List <Vehicle> vehicleList=vehicleRepository.findAllByReservedBy(email);
        List <Vehicle> resVehicleList = new ArrayList<>();
        if(vehicleList.size()>0){
            vehicleList.forEach(vehicle->{
                if(vehicle.getStatus().equals("Reserved")){
                    resVehicleList.add(vehicle);
                }
            });
            return new ResponseEntity<>(resVehicleList,HttpStatus.OK);
        }

        return new ResponseEntity<>("Error",HttpStatus.BAD_REQUEST);
    }


    public ResponseEntity<?> getAllReservedVehicles(){
        List <Vehicle> vehicleList= vehicleRepository.findAllByStatus("Reserved");
        return new ResponseEntity<>(vehicleList,HttpStatus.OK);
    }

    public VehicleResponse findResVehicleById(long id) {
        Optional<Vehicle> vehicle1= vehicleRepository.findById(id);
        Vehicle vehicle=vehicle1.get();
        VehicleResponse vehicleResponse=new VehicleResponse();
        vehicleResponse.setId(vehicle.getId());
        vehicleResponse.setVehicleMake(vehicle.getVehicleMake());
        vehicleResponse.setVehicleModel(vehicle.getVehicleModel());
        vehicleResponse.setVehicleType(vehicle.getVehicleType());
        vehicleResponse.setVehicleExteriorColor(vehicle.getVehicleExteriorColor());
        vehicleResponse.setVehicleInteriorColor(vehicle.getVehicleInteriorColor());
        vehicleResponse.setVehicleYom(vehicle.getVehicleYom());
        vehicleResponse.setVehicleDescription(vehicle.getVehicleDescription());
        vehicleResponse.setVehicleFuel(vehicle.getVehicleFuel());
        vehicleResponse.setVehicleTransmission(vehicle.getVehicleTransmission());
        vehicleResponse.setVehicleMileage(vehicle.getVehicleMileage());
        vehicleResponse.setVehiclePrice(vehicle.getVehiclePrice());
        vehicleResponse.setStatus(vehicle.getStatus());
        vehicleResponse.setReservedBy(vehicle.getReservedBy());
        vehicleResponse.setIsAd(vehicle.getIsAd());
        vehicleResponse.setIsAdApproved(vehicle.getIsAdApproved());
        vehicleResponse.setCovrimg(vehicle.getCovrimg());
        vehicleResponse.setImageList(vehicle.retrieveImagesArray());
        return vehicleResponse;
    }

    public boolean cancelReservation(VehicleDto vehicleDto) {
        Optional <Vehicle> vehicle=vehicleRepository.findById(vehicleDto.getVehicleId());
        if(vehicle.isPresent())
        {
            Vehicle vehicle1=vehicle.get();
            vehicle1.setStatus(vehicleDto.getStatus());
            vehicleRepository.save(vehicle1);
            return true;
        }
        return false;
    }

//    public ResponseEntity<?> addToSavedList(SavedListDto savedListDto) {
//        Optional <User> userOptional=userRepo.findById(savedListDto.getUseremail());
//        User user=userOptional.get();
//        Optional <SavedList> savedList=savedListRepo.findByUser(user);
//
//        if(savedList.isPresent()){
//            SavedList savedList1=savedList.get();
//            Optional<Vehicle> vehicle=vehicleRepository.findById(savedListDto.getVehicleid());
//            if(vehicle.isPresent()){
//                Vehicle vehicle1=vehicle.get();
//                savedList1.setVehicle(vehicle1);
//            }
//        }
//
//    }

//    public ResponseEntity<?> createCart(SavedListDto) {
//        int productQuantity = cart.getQuantity();
//        boolean itemAlreadyExists = false;
//        if (user == null) {
//            throw new GenericException("User not found.", HttpStatus.UNAUTHORIZED);
//        }
//        Product product = productRepository.findById(productId).orElse(null);
//        if (product == null) {
//            throw new GenericException("Product not found", HttpStatus.BAD_REQUEST);
//        }
//        List<Cart> availableItems = cartRepository.findByUserAndProduct(user, product);
//        if (availableItems.size() > 0) {
//            for (Cart availableItem : availableItems) {
//                if (cart.getSize().equals(availableItem.getSize())) {
//                    cart = availableItem;
//                    cart.setQuantity(cart.getQuantity() + productQuantity);
//                    itemAlreadyExists = true;
//                    break;
//                }
//            }
//        }
//        if (product.getStock() < cart.getQuantity()) {
//            throw new GenericException("Product only has " + product.getStock() + " pieces available.", HttpStatus.BAD_REQUEST);
//        }
//        cart.setTotalPrice(product.getPrice() * cart.getQuantity());
//        if (!itemAlreadyExists) {
//            cart.setProduct(product);
//            cart.setUser(user);
//        }
//        cartRepository.save(cart);
//
//        List<Cart> userCart = cartRepository.findByUser(user);
//        return ResponseEntity.ok(new CartResponse(userCart));
//    }
//    public ResponseEntity<?> removeFromSavedList(long id) {
//    }

//export const ViewSavedVehicles=()=>{};
//export const AddToSavedVehicles=()=>{};
//export const SearchVehicle=()=>{};

//    public ResponseEntity<?> getProductsByName(String name, Pageable pageable) {
//        Page<Product> products = productRepository.findAllByNameContaining(name, pageable);
//        return ResponseEntity.ok(new ProductListResponse(products.getTotalPages(), products.getNumber(), products.getContent()));
//    }
}
