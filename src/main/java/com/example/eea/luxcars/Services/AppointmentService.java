package com.example.eea.luxcars.Services;

import com.example.eea.luxcars.DTO.AppointmentDto;
import com.example.eea.luxcars.Entities.Appointments;
import com.example.eea.luxcars.Entities.User;
import com.example.eea.luxcars.Entities.Vehicle;
import com.example.eea.luxcars.Repositories.AppointmentRepo;
import com.example.eea.luxcars.Repositories.UserRepo;
import com.example.eea.luxcars.Repositories.VehicleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AppointmentService {

    @Autowired
    AppointmentRepo appointmentRepo;

    @Autowired
    UserRepo userRepo;

    @Autowired
    VehicleRepo vehicleRepo;

    public Boolean makeAppointment(AppointmentDto appointmentDto){

        Optional <Appointments> appointmentsOptional=appointmentRepo.findById(appointmentDto.getId());
        Optional <User> userOptional=userRepo.findById(appointmentDto.getUseremail());
        Optional <Vehicle> vehicleOptional=vehicleRepo.findById(appointmentDto.getVehicleid());

        if(!appointmentsOptional.isPresent()) {
                Appointments appointments=new Appointments();
            if (userOptional.isPresent() && vehicleOptional.isPresent()) {

                User user = userOptional.get();
                Vehicle vehicle = vehicleOptional.get();

                appointments.setAppointmentDateTime(appointmentDto.getAppointmentDateTime());
                appointments.setMessage(appointmentDto.getMessage());
                appointments.setUser(user);
                appointments.setStatus("Pending");
                appointments.setVehicle(vehicle);

            }
            appointmentRepo.save(appointments);
            return true;
        }
        return false;
    }

    public ResponseEntity<?> viewAllAppointment(){
        List<AppointmentDto> appointmentList=new ArrayList<>();
        appointmentRepo.findAll().forEach(appointments ->
        {
            AppointmentDto appointmentDto=new AppointmentDto();
            appointmentDto.setAppointmentDateTime(appointments.getAppointmentDateTime());
            appointmentDto.setId(appointments.getId());
            appointmentDto.setMessage(appointments.getMessage());
            appointmentDto.setUseremail(appointments.getUser().getEmail());
            appointmentDto.setVehicleid(appointments.getVehicle().getId());
            appointmentDto.setStatus(appointments.getStatus());
            appointmentList.add(appointmentDto);
        });
        return new ResponseEntity<>(appointmentList, HttpStatus.OK);
    }

    public ResponseEntity<?> respondToAppointment(AppointmentDto appointmentDto){

        Optional <Appointments> appointmentsOptional=appointmentRepo.findById(appointmentDto.getId());
        if(appointmentsOptional.isPresent()){
            Appointments appointments=appointmentsOptional.get();
            appointments.setStatus(appointmentDto.getStatus());
            appointmentRepo.save(appointments);
            return new ResponseEntity<>("Status Updated",HttpStatus.OK);
        }
        return new ResponseEntity<>("Error!",HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ResponseEntity<?> findAllByUser(String email){
        Optional<User> userOptional=userRepo.findById(email);
        if(userOptional.isPresent()){
            User user=userOptional.get();
            List <Appointments> appointmentsList=new ArrayList<>();
            appointmentRepo.findAllByUser(user).forEach(appointments -> appointmentsList.add(appointments));
            return new ResponseEntity<>(appointmentsList,HttpStatus.OK);
        }
        return new ResponseEntity<>("Error!",HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ResponseEntity<?> cancelAppointment(Long id){

        Optional <Appointments> appointmentsOptional=appointmentRepo.findById(id);
        if(appointmentsOptional.isPresent()){
            Appointments appointments=appointmentsOptional.get();
            appointments.setStatus("Cancelled");
            appointmentRepo.save(appointments);
            return new ResponseEntity<>("Status Updated",HttpStatus.OK);
        }
        return new ResponseEntity<>("Error!",HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
