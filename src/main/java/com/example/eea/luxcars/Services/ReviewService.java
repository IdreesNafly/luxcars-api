package com.example.eea.luxcars.Services;

import com.example.eea.luxcars.DTO.ReviewDto;
import com.example.eea.luxcars.Entities.Reviews;
import com.example.eea.luxcars.Entities.User;
import com.example.eea.luxcars.Repositories.ReviewRepo;
import com.example.eea.luxcars.Repositories.UserRepo;
import com.example.eea.luxcars.Repositories.VehicleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReviewService {

    @Autowired
    UserRepo userRepo;
    @Autowired
    ReviewRepo reviewRepo;
    @Autowired
    VehicleRepo vehicleRepo;

    public Boolean addReview(ReviewDto reviewDto) throws Exception {

        Optional<User> userOptional=userRepo.findById(reviewDto.getUserid());

        if(!userOptional.isPresent()){
            throw new Exception("User Not found");
        }

        User user=userOptional.get();
        //Reviews reviews=reviewRepo.findByVehicleAndUser(vehicle,user);
        Reviews reviews = new Reviews();
            reviews.setDescription(reviewDto.getDescription());
           // reviews.setHeader(reviewDto.getHeader());
            reviews.setRating(reviewDto.getRating());
            reviews.setUser(user);
            reviewRepo.save(reviews);
            return true;
    }

//    public List <Reviews>  getVehicleReviews(long Id) {
//
//        Optional <Vehicle> vehicleOptional=vehicleRepo.findById(Id);
//        if(vehicleOptional.isPresent()){
//            Vehicle vehicle=vehicleOptional.get();
////            List <Reviews> reviews=reviewRepo.findByVehicle(vehicle);
////            return  reviews;
//        }
//       return null;
//    }

    public Boolean editReview(ReviewDto reviewDto){

        Optional <Reviews> reviews=reviewRepo.findById(reviewDto.getId());

        if(reviews.isPresent()){
            Optional<User> user=userRepo.findByEmail(reviewDto.getUserid());
            User user1=user.get();
            Reviews reviews1=reviews.get();
            reviews1.setUser(user1);
            reviews1.setRating(reviewDto.getRating());
            reviews1.setDescription(reviewDto.getDescription());
            //reviews1.setHeader(reviewDto.getHeader());
            reviewRepo.save(reviews1);
            return true;
        }
        return false;
    }

    public Boolean deleteReview(long id){
        Optional <Reviews> reviewsOptional=reviewRepo.findById(id);

        if(reviewsOptional.isPresent()){
            Reviews reviews=reviewsOptional.get();
            reviewRepo.deleteById(reviews.getId());
            return true;
        }
        return false;
    }

    public ResponseEntity<?> getAllReviews(){
        List<ReviewDto> reviewsList=new ArrayList<>();
        reviewRepo.findAll().forEach(reviews ->{
                    ReviewDto reviewDtos=new ReviewDto();
                    reviewDtos.setDescription(reviews.getDescription());
                    reviewDtos.setId(reviews.getId());
                    reviewDtos.setUserid(reviews.getUser().getEmail());
                    reviewDtos.setCreatedDate(reviews.getReviewCreatedOn().toString());
                    reviewDtos.setRating(reviews.getRating());
                    reviewsList.add(reviewDtos);
                }
                );
        return new ResponseEntity<>(reviewsList, HttpStatus.OK);
    }


}
