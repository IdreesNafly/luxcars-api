package com.example.eea.luxcars.Services;

import com.example.eea.luxcars.DTO.SavedListDto;
import com.example.eea.luxcars.Entities.SavedList;
import com.example.eea.luxcars.Entities.User;
import com.example.eea.luxcars.Entities.Vehicle;
import com.example.eea.luxcars.Repositories.SavedListRepo;
import com.example.eea.luxcars.Repositories.UserRepo;
import com.example.eea.luxcars.Repositories.VehicleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SavedListService {

    @Autowired
    VehicleRepo vehicleRepo;

    @Autowired
    SavedListRepo savedListRepo;

    @Autowired
    UserRepo userRepo;

    public ResponseEntity<?>addToSavedList(SavedListDto savedListDto){
        Optional<Vehicle> vehicleOptional= vehicleRepo.findById(savedListDto.getVehicleid());
        Optional<User> userOptional= userRepo.findById(savedListDto.getUserid());

        if(userOptional.isPresent()){
            User user=userOptional.get();
            Vehicle vehicle=vehicleOptional.get();

            List <SavedList> savedListItems=savedListRepo.findByUserAndVehicle(user,vehicle);
            if(savedListItems.size()>0){
                return  new ResponseEntity<>("Vehicle Already Exists in your Saved List",HttpStatus.BAD_REQUEST);
            }
            SavedList savedList=new SavedList();
            savedList.setUser(user);
            savedList.setVehicle(vehicle);
            savedListRepo.save(savedList);
            return new ResponseEntity<>("Added To Saved List",HttpStatus.OK);
        }
        return new ResponseEntity<>("Error Adding to Save list",HttpStatus.BAD_REQUEST);
    }

//    public ResponseEntity <?> addforsadedlist(SavedListDto savedListDto){
//        Optional <User> userOptional=userRepo.findById(savedListDto.getUserid());
//        Optional<Vehicle> vehicleOptional=vehicleRepo.findById(savedListDto.getVehicleid());
//        Vehicle vehicle=vehicleOptional.get();
//
//        if(userOptional.isPresent()){
//            User user=userOptional.get();
//            Optional <SavedList> savedListOptional= Optional.ofNullable(savedListRepo.findByUser(user));
//            if (savedListOptional.isPresent()){
//                SavedList existingSavedList=savedListOptional.get();
//                List <Vehicle> vehicleList=existingSavedList.getVehicleList();
//
//                if(vehicleList.contains(vehicle)){
//                    return new ResponseEntity<>("Vehicle Already Added to Saved List",HttpStatus.BAD_REQUEST);
//                }
//                vehicleList.add(vehicle);
//                existingSavedList.setVehicleList(vehicleList);
//                savedListRepo.save(existingSavedList);
//                return new ResponseEntity<>("Vehicle Added to Saved List",HttpStatus.OK);
//            }
//            SavedList newSavedList=new SavedList();
//            newSavedList.setUser(user);
//            List <Vehicle> newVehicleList=newSavedList.getVehicleList();
//            newVehicleList.add(vehicle);
//            newSavedList.setVehicleList(newVehicleList);
//            savedListRepo.save(newSavedList);
//            return new ResponseEntity<>("Vehicle Added to Saved List",HttpStatus.OK);
//        }
//        return new ResponseEntity<>("Error Adding to Saved List",HttpStatus.BAD_REQUEST);
//    }

    public ResponseEntity<?>getSavedList(String userid){
        Optional <User> userOptional=userRepo.findById(userid);

        if(userOptional.isPresent()){
            User user=userOptional.get();
            List <SavedList> savedLists= savedListRepo.findAllByUser(user);
            return new ResponseEntity<>(savedLists, HttpStatus.OK);
        }
        return new ResponseEntity<>("Error", HttpStatus.BAD_REQUEST);


    }

    public ResponseEntity<?> removeFromSavedList(Long id){
        Optional <SavedList> savedListOptional=savedListRepo.findById(id);
        if(savedListOptional.isPresent()){
            SavedList savedList=savedListOptional.get();
            savedListRepo.delete(savedList);
            return new ResponseEntity<>("Removed From Saved List",HttpStatus.OK);

        }
        return new ResponseEntity<>("Erro Removing",HttpStatus.OK);
    }
}
