package com.example.eea.luxcars.Services;

import com.example.eea.luxcars.Entities.User;
import com.example.eea.luxcars.Repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepo userRepo;

    public ResponseEntity<?> getAllUsers(){

        List<User> userList=new ArrayList<>();
        userRepo.findAll().forEach(user -> userList.add(user));
        return new ResponseEntity<>(userList, HttpStatus.OK);
    }

    public ResponseEntity<?> deleteUser(String id){
        Optional <User> userOptional=userRepo.findById(id);

        if(userOptional.isPresent()){
            userRepo.deleteById(id);
            return new ResponseEntity<>("Deleted",HttpStatus.OK);
        }

        return new ResponseEntity<>("User Not found",HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
