package com.example.eea.luxcars.Controllers;

import com.example.eea.luxcars.DTO.ReviewDto;
import com.example.eea.luxcars.Services.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/reviews")
public class ReviewController {

    @Autowired
    ReviewService reviewService;

    @PostMapping("/addReview")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public Boolean AddReview(@RequestBody ReviewDto reviewDto) throws Exception {
        return reviewService.addReview(reviewDto);
    }

    @DeleteMapping("/deleteReview/{id}")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public Boolean deleteReview(@PathVariable long id){
        return reviewService.deleteReview(id);
    }

    @PutMapping("/updateReview")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public Boolean updateReview(@RequestBody ReviewDto reviewDto){
        return reviewService.editReview(reviewDto);
    }

//    @GetMapping("/getReviews/{id}")
//    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
//    public List getReviewsbyVehicle(@PathVariable long id){
//        return reviewService.getVehicleReviews(id);
//    }

    @GetMapping("/getAllReviews")
    public ResponseEntity<?> getAllReviews(){
        return reviewService.getAllReviews();
    }




}
