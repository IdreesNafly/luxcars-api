package com.example.eea.luxcars.Controllers;

import com.example.eea.luxcars.DTO.AppointmentDto;
import com.example.eea.luxcars.Services.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/appointments")
public class AppointmentController {

    @Autowired
    AppointmentService appointmentService;

    @PostMapping("/makeAppointments")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public boolean makeAppointments(@RequestBody AppointmentDto appointmentDto){
        return appointmentService.makeAppointment(appointmentDto);
    }

    @GetMapping("/getAllAppointment")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getAllAppointments(){
        return appointmentService.viewAllAppointment();
    }

    @PutMapping("/respondToAppointment")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> respondtoAppointment(@RequestBody AppointmentDto appointmentDto){
        return appointmentService.respondToAppointment(appointmentDto);
    }

    @GetMapping("/getAppointmentByUser/{email}")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getAppointmentByUser(@PathVariable String email){
        return appointmentService.findAllByUser(email);
    }

    @PutMapping("/cancelAppointment/{id}")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> respondtoAppointment(@PathVariable Long id){
        return appointmentService.cancelAppointment(id);
    }



}
