package com.example.eea.luxcars.Controllers;

import com.example.eea.luxcars.DTO.VehicleAd;
import com.example.eea.luxcars.DTO.VehicleDto;
import com.example.eea.luxcars.DTO.VehicleResponse;
import com.example.eea.luxcars.Entities.Vehicle;
import com.example.eea.luxcars.Repositories.VehicleRepo;
import com.example.eea.luxcars.Services.VehicleService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("api/vehicle")
public class VehicleController {

    @PostMapping("/addVehicle")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Boolean AddVehicle(@RequestParam("files") MultipartFile [] files, @RequestParam("vehicle") String vehicle) throws IOException {

        Gson g = new Gson();
        VehicleAd vehicleAd = g.fromJson(vehicle, VehicleAd.class);

        Vehicle vehicle1=new Vehicle();
        vehicle1.setVehiclePrice(vehicleAd.getVehiclePrice());
        vehicle1.setVehicleMake(vehicleAd.getVehicleMake());
        vehicle1.setVehicleModel(vehicleAd.getVehicleModel());
        vehicle1.setVehicleDescription(vehicleAd.getVehicleDescription());
        vehicle1.setVehicleExteriorColor(vehicleAd.getVehicleExteriorColor());
        vehicle1.setVehicleInteriorColor(vehicleAd.getVehicleInteriorColor());
        vehicle1.setVehicleType(vehicleAd.getVehicleType());
        vehicle1.setVehicleYom(vehicleAd.getVehicleYom());
        vehicle1.setVehicleFuel(vehicleAd.getVehicleFuel());
        vehicle1.setVehicleTransmission(vehicleAd.getVehicleTransmission());
        vehicle1.setVehicleMileage(vehicleAd.getVehicleMileage());
        return vehicleService.addVehicle(vehicle1,null,files);
    }

    @Autowired
    VehicleService vehicleService;

    @Autowired
    VehicleRepo vehicleRepo;



    @PostMapping("/postAd")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public Boolean postAdVehicle(@RequestParam("files") MultipartFile [] files, @RequestParam("vehicle") String vehicledet){

        Gson g = new Gson();
        VehicleAd vehicle = g.fromJson(vehicledet, VehicleAd.class);

        Vehicle vehicle1=new Vehicle();
        vehicle1.setVehiclePrice(vehicle.getVehiclePrice());
        vehicle1.setVehicleMake(vehicle.getVehicleMake());
        vehicle1.setVehicleModel(vehicle.getVehicleModel());
        vehicle1.setVehicleDescription(vehicle.getVehicleDescription());
        vehicle1.setVehicleExteriorColor(vehicle.getVehicleExteriorColor());
        vehicle1.setVehicleInteriorColor(vehicle.getVehicleInteriorColor());
        vehicle1.setVehicleType(vehicle.getVehicleType());
        vehicle1.setVehicleYom(vehicle.getVehicleYom());
        vehicle1.setVehicleFuel(vehicle.getVehicleFuel());
        vehicle1.setVehicleTransmission(vehicle.getVehicleTransmission());
        vehicle1.setVehicleMileage(vehicle.getVehicleMileage());

        String email=vehicle.getUserid();

        return vehicleService.addVehicle(vehicle1,email,files);
    }

    @GetMapping("/getAllVehicles")
    //@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public List getAllVehicles(){
        return vehicleService.getAllVehicle();
    }

    @GetMapping("/getResVehicle")
    public List getAllResVehicles(){
        return vehicleService.getAllVehicleResponse();
    }

    @GetMapping("/getVehicleById/{id}")
    //@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public Vehicle getVehicleById(@PathVariable long id){
        return vehicleService.findVehicleById(id);
    }

    @GetMapping("/getResVehicleById/{id}")
    //@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public VehicleResponse getResVehicleById(@PathVariable long id){
        return vehicleService.findResVehicleById(id);
    }

    @DeleteMapping("/deleteVehicle/{id}")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> deleteVehicle(@PathVariable long id){
        return vehicleService.deleteVehicle(id);
    }

    @PutMapping("/updateVehiclePrice")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public boolean updateVehiclePrice(@RequestBody VehicleDto vehicleDto){
        return vehicleService.updateVehiclePrice(vehicleDto);
    }

    @PutMapping("/updateVehicleStatus")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public boolean updateVehicleStatus(@RequestBody VehicleDto vehicleDto){
        return vehicleService.updateVehicleStatus(vehicleDto);
    }
    @PutMapping("/cancelReservation")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public boolean cancelReservation(@RequestBody VehicleDto vehicleDto){
        return vehicleService.cancelReservation(vehicleDto);
    }

    @PutMapping("/approveAd/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public boolean approveAd(@PathVariable long id){
        return vehicleService.approveAd(id);
    }

    @GetMapping("/getAllVehicleByUser/{email}")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public List getAllVehicleByUser(@PathVariable String email){
        return vehicleService.getAllVehiclebyUser(email);
    }

    @GetMapping("/getAllAdVehicles")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getAllAdVehicles(){
        return vehicleService.viewAllAdVehicles();
    }


    @GetMapping("/getAllReservedVehicles")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getAllReservedVehicle(){
        return vehicleService.getAllReservedVehicles();
    }

    @GetMapping("/getReservedVehicles/{email}")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getReservedVehicles(@PathVariable String email){
        return vehicleService.getReservedVehicles(email);
    }

    @GetMapping("/search/{make}/{model}/{year}")
    public ResponseEntity<?> search(@PathVariable String make, @PathVariable String model,@PathVariable String year){
        List <Vehicle> vehicleList=vehicleRepo.findAllByVehicleMakeAndVehicleModelAndVehicleYom(make,model,year);
        return new ResponseEntity<>(vehicleList, HttpStatus.OK);
    }

    @GetMapping("/search/{make}")
    public ResponseEntity<?> search(@PathVariable String make){
        List <Vehicle> vehicleList=vehicleRepo.findAllByVehicleMake(make);
        return new ResponseEntity<>(vehicleList, HttpStatus.OK);
    }

    @GetMapping("/searchByType/{type}")
    public ResponseEntity<?> searchByType(@PathVariable String type){
        List <Vehicle> vehicleList=vehicleRepo.findAllByVehicleType(type);
        return new ResponseEntity<>(vehicleList, HttpStatus.OK);
    }

    @GetMapping("/getAllMake")
    public List <String> getAllMake(){
        return vehicleRepo.getAllVehicleMake();
    }

    @GetMapping("/getAllModel")
    public List <String> getAllModel(){
        return vehicleRepo.getAllVehicleModel();
    }

    @GetMapping("/getAllYear")
    public List <String> getAllYear(){
        return vehicleRepo.getAllVehicleYear();
    }

//
//    @PostMapping("/addtosaved")
//    public ResponseEntity<?> addToSavedList(@RequestBody SavedListDto savedListDto){
//        return vehicleService.addToSavedList(savedListDto);
//    }
//
//    @DeleteMapping("/deletefromsaved/{id}")
//    public ResponseEntity<?> removeFromSavedList(@PathVariable long id){
//        return vehicleService.removeFromSavedList(id);
//    }





}
