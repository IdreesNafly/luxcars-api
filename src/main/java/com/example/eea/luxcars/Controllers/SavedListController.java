package com.example.eea.luxcars.Controllers;

import com.example.eea.luxcars.DTO.SavedListDto;
import com.example.eea.luxcars.Services.SavedListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/savedlist")
@CrossOrigin("*")
public class SavedListController {

    @Autowired
    SavedListService savedListService;

    @PostMapping("/addtosavedlist")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> add(@RequestBody SavedListDto savedListDto){
        return savedListService.addToSavedList(savedListDto);
    }

    @GetMapping("/get/{id}")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> get(@PathVariable String id){
        return savedListService.getSavedList(id);
    }

    @DeleteMapping("/deleteItem/{id}")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> deleteFromSavedList(@PathVariable Long id){
        return savedListService.removeFromSavedList(id);
    }

}
