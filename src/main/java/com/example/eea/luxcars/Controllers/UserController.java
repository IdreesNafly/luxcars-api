package com.example.eea.luxcars.Controllers;

import com.example.eea.luxcars.Entities.User;
import com.example.eea.luxcars.Repositories.UserRepo;
import com.example.eea.luxcars.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("api/users")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserRepo userRepo;

    @GetMapping("/getAllUser")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getAllUser(){
        return userService.getAllUsers();
    }

    @DeleteMapping("/deleteUser/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> deleteuser(@PathVariable String id){
        return userService.deleteUser(id);
    }


    @GetMapping("/getUser/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<?> getUser(@PathVariable String id){
        Optional<User> user= userRepo.findByEmail(id);
        if(user.isPresent()){
            User user1=user.get();
            return new ResponseEntity<>(user1, HttpStatus.OK);
        }
        return new ResponseEntity<>("Error",HttpStatus.BAD_REQUEST);
    }
}
